import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Homepage from '../components/homepage';
import {MovieList} from '../components/movies';

const Routes = () => (
    <Switch>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/peliculas" component={MovieList} />
        {/* <Route render={() => <p>Página 404</p>}/> */}
        <Redirect to="/" />
    </Switch>
);

export default Routes;