import React from 'react';
import './index.scss'

const Footer = () => (
    <>
        <footer className="footer-container">
            <p>Todos los derechos reservados. William Méndez 2020</p>
        </footer>
    </>
);

export default Footer;