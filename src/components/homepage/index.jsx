import React from 'react';
import './index.scss';
import {Link} from 'react-router-dom';
import Logo from '../../assets/logo.svg';

const Homepage = () => (
    <>
        <div className="welcome-container">
            <img src={Logo} alt="Logo de Elementary"/>
            <p className="welcome-text">
                Bienvenido a la mejor plataforma de películas en Elementary
            </p>
            <button className="welcome-button">
                <Link to="/peliculas">
                    Ver películas
                </Link>
            </button>
        </div>
    </>
);

export default Homepage;